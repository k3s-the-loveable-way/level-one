#!/bin/sh
SECONDS=0

# Read configuration file
eval $(cat vm.config)

# Create Ubuntu VM
echo "🚧 VMs creation..."

multipass launch --name ${vm_name}-1 --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
  --cloud-init ./cloud-init.yaml

multipass launch --name ${vm_name}-2 --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
  --cloud-init ./cloud-init.yaml

multipass launch --name ${vm_name}-3 --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
  --cloud-init ./cloud-init.yaml

# Install K3s
echo "1️⃣ Initialize 📦 K3s on ${vm_name}-1 ..."

multipass mount config ${vm_name}-1:config

multipass --verbose exec ${vm_name}-1 -- sudo -- bash <<EOF
curl -sfL https://get.k3s.io | sh -s - --node-name one
EOF

TOKEN=$(multipass exec ${vm_name}-1 sudo cat /var/lib/rancher/k3s/server/node-token)
IP=$(multipass info ${vm_name}-1 | grep IPv4 | awk '{print $2}')

echo "😃 📦 K3s initialized on ${vm_name}-1 ✅"
echo "🖥 IP: ${IP}"

# Generate config file for kubectl
multipass --verbose exec ${vm_name}-1 -- sudo -- bash <<EOF
cat /etc/rancher/k3s/k3s.yaml > config/k3s.yaml
sed -i "s/127.0.0.1/$IP/" config/k3s.yaml
echo "🎉 K3S installation complete 🍾 on ${vm_name}-1"
EOF

echo "🤖 joining nodes 2 & 3 ..."

echo "2️⃣ Initialize 📦 K3s on ${vm_name}-2 and join ${vm_name}-1 ..."
# Joining node2
multipass --verbose exec ${vm_name}-2 -- sudo -- bash <<EOF
curl -sfL https://get.k3s.io | K3S_URL="https://${IP}:6443" K3S_TOKEN="${TOKEN}" sh -s - --node-name two
echo "🎉 ${vm_name}-2 joined"
EOF

echo "3️⃣ Initialize 📦 K3s on ${vm_name}-3 and join ${vm_name}-1 ..."
# Joining node3
multipass --verbose exec ${vm_name}-3 -- sudo -- bash <<EOF
curl -sfL https://get.k3s.io | K3S_URL="https://${IP}:6443" K3S_TOKEN="${TOKEN}" sh -s - --node-name three
echo "🎉 ${vm_name}-3 joined"
EOF

duration=$SECONDS
echo "Duration: $(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."