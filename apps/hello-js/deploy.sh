#!/bin/sh
# Export KUBECONFIG for kubectl
export KUBECONFIG=$PWD/../../config/k3s.yaml
# Load the configuration file of the VM
eval $(cat $PWD/../../vm.config)
# Use the main vm (the first one)
vm_name=${vm_name}-1
# Get the IP of the VM
IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

export SUB_DOMAIN="${IP}.nip.io"


# Default http poets
export CONTAINER_PORT=${CONTAINER_PORT:-8080}
export EXPOSED_PORT=${EXPOSED_PORT:-80}

# Commit the update
COMMIT_MESSAGE="👋 update and 🚀 deploy"
git add .; git commit -m "${COMMIT_MESSAGE}"; 

export TAG=$(git rev-parse --short HEAD)
export BRANCH=$(git symbolic-ref --short HEAD)
export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))-${BRANCH}

# 🐳 Docker information
export DOCKER_USER="k33g"
export IMAGE_NAME="${APPLICATION_NAME}-img"
export IMAGE="${DOCKER_USER}/${IMAGE_NAME}:${TAG}"

# 🐳 Build and Push the Docker image
docker build -t ${IMAGE_NAME} .
docker tag ${IMAGE_NAME} ${IMAGE}
docker push ${IMAGE}

export HOST="${APPLICATION_NAME}.${SUB_DOMAIN}"
export NAMESPACE="default"

# 🚀 Deploy to K3S
rm ./kube/*.yaml

envsubst < ./deploy.template.yaml > ./kube/deploy.${TAG}.yaml

kubectl apply -f ./kube/deploy.${TAG}.yaml -n ${NAMESPACE}

echo "🌍 http://${HOST}"
