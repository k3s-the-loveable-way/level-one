#!/bin/sh
export KUBECONFIG=$PWD/../../config/k3s.yaml

eval $(cat $PWD/../../vm.config)

# use the main vm
vm_name=${vm_name}-1

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
export SUB_DOMAIN="${IP}.nip.io"

export DOCKER_USER="k33g"

COMMIT_MESSAGE="👋 update and 🚀 deploy"

git add .; git commit -m "${COMMIT_MESSAGE}"; 

export CONTAINER_PORT=${CONTAINER_PORT:-8080}
export EXPOSED_PORT=${EXPOSED_PORT:-80}

export TAG=$(git rev-parse --short HEAD)
export BRANCH=$(git symbolic-ref --short HEAD)
export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))-${BRANCH}
# 👋

export IMAGE_NAME="${APPLICATION_NAME}-img"
export IMAGE="${DOCKER_USER}/${IMAGE_NAME}:${TAG}"

# === Build ===
docker build -t ${IMAGE_NAME} .
docker tag ${IMAGE_NAME} ${IMAGE}
docker push ${IMAGE}

export HOST="${APPLICATION_NAME}.${SUB_DOMAIN}"

export NAMESPACE="default"

export REDIS_PORT="6379"
export REDIS_HOST="redis-mother.database"
export REDIS_PASSWORD=""

# === Deploy ===
rm ./kube/*.yaml

envsubst < ./deploy.template.yaml > ./kube/deploy.${TAG}.yaml

kubectl apply -f ./kube/deploy.${TAG}.yaml -n ${NAMESPACE}

echo "🌍 http://${HOST}"
