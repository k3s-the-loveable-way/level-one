#!/bin/sh
export KUBECONFIG=$PWD/../../config/k3s.yaml

NAMESPACE="default"

# see https://stackoverflow.com/questions/38216278/update-k8s-configmap-or-secret-without-deleting-the-existing-one

kubectl create configmap javascript-function-map \
  --from-file function.js -o yaml \
  -n ${NAMESPACE}

