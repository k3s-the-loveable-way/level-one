const express = require('express')
const redis = require('redis')

const app = express()
const port = process.env.PORT || 8080

app.use(express.static('public'))
app.use(express.json())

console.log("REDIS_PORT", process.env.REDIS_PORT)
console.log("REDIS_HOST", process.env.REDIS_HOST)
console.log("REDIS_PASSWORD", process.env.REDIS_PASSWORD)


let redisClient = redis.createClient({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST,
  password: process.env.REDIS_PASSWORD
})


function fancyName() {
  let adjs = ["autumn", "hidden", "bitter", "misty", "silent", "empty", "dry",
  "dark", "summer", "icy", "delicate", "quiet", "white", "cool", "spring",
  "winter", "patient", "twilight", "dawn", "crimson", "wispy", "weathered",
  "blue", "billowing", "broken", "cold", "damp", "falling", "frosty", "green",
  "long", "late", "lingering", "bold", "little", "morning", "muddy", "old",
  "red", "rough", "still", "small", "sparkling", "throbbing", "shy",
  "wandering", "withered", "wild", "black", "young", "holy", "solitary",
  "fragrant", "aged", "snowy", "proud", "floral", "restless", "divine",
  "polished", "ancient", "purple", "lively", "nameless"]

  , nouns = ["waterfall", "river", "breeze", "moon", "rain", "wind", "sea",
  "morning", "snow", "lake", "sunset", "pine", "shadow", "leaf", "dawn",
  "glitter", "forest", "hill", "cloud", "meadow", "sun", "glade", "bird",
  "brook", "butterfly", "bush", "dew", "dust", "field", "fire", "flower",
  "firefly", "feather", "grass", "haze", "mountain", "night", "pond",
  "darkness", "snowflake", "silence", "sound", "sky", "shape", "surf",
  "thunder", "violet", "water", "wildflower", "wave", "water", "resonance",
  "sun", "wood", "dream", "cherry", "tree", "fog", "frost", "voice", "paper",
  "frog", "smoke", "star"]

  return adjs[Math.floor(Math.random()*(adjs.length-1))]+"_"+nouns[Math.floor(Math.random()*(nouns.length-1))]
}

let fancy_name = fancyName()

app.get('/api/hello', (req, res) => {
	res.send({
		message: `👋 Hello world 🌍`,
		pod: fancy_name
	})
})

app.get('/api/function/:message', (req, res) => {
	res.send({
		message: require('./function').handle(req.params.message),
		pod: fancy_name
	})
})


/*
{
  id: 1234,
  name: "john doe",
  avatar: "🤖" 
  }
}
*/
app.post('/api/buddy', (req, res) => {
  try {
    redisClient.hmset(req.body.id, "name", req.body.name, "avatar", req.body.avatar, (error, value) => {
      if(error) console.log("😡", error, value)
      if(value=="OK") console.log("😃", value)
      console.log("📦", req.body)
      res.status(200).send({
        error, value, pod: fancy_name
      })
    })
  } catch (error) {
    console.log("😡", error)
    res.status(415)
      .header('Content-Type', 'application/json')
      .send(JSON.stringify(error))
  }
})

app.get('/api/buddy/:id', (req, res) => {
  try {
    redisClient.hgetall(req.params.id, (error, value) => {
      if(error) console.log("😡", error, value)
      if(value) console.log("😃", value)
      res.status(200).send({
        error, value, pod: fancy_name
      })
    })
  } catch (error) {
    console.log("😡", error)
    res.status(415)
      .header('Content-Type', 'application/json')
      .send(JSON.stringify(error))
  }
})

app.listen(port, () => console.log(`🌍 webapp ${fancy_name} is listening on port ${port}!`))



