#!/bin/sh
export KUBECONFIG=$PWD/../../config/k3s.yaml

NAMESPACE="default"

# see https://stackoverflow.com/questions/38216278/update-k8s-configmap-or-secret-without-deleting-the-existing-one

kubectl create configmap javascript-function-map \
  --from-file function.js -o yaml \
  --dry-run | kubectl replace -n ${NAMESPACE} -f -

export BRANCH=$(git symbolic-ref --short HEAD)
export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))-${BRANCH}

kubectl -n default rollout restart deployment/${APPLICATION_NAME}

