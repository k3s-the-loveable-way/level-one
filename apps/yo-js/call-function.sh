#!/bin/sh
export KUBECONFIG=$PWD/../../config/k3s.yaml
eval $(cat $PWD/../../vm.config)
# use the main vm
vm_name=${vm_name}-1
IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
export SUB_DOMAIN="${IP}.nip.io"

export BRANCH=$(git symbolic-ref --short HEAD)
export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))-${BRANCH}

export HOST="${APPLICATION_NAME}.${SUB_DOMAIN}"

http GET http://${HOST}/api/function/hello
http GET http://${HOST}/api/function/hey


