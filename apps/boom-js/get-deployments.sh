#!/bin/bash
export KUBECONFIG=$PWD/../../config/k3s.yaml
eval $(cat $PWD/../../vm.config)
# use the main vm
vm_name=${vm_name}-1

export NAMESPACE="default"
export BRANCH=$(git symbolic-ref --short HEAD)
export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))-${BRANCH}

kubectl get deployments -n $NAMESPACE

