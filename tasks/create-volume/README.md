Avec K3S vous avez par défaut ce que l'on appelle le "Local Path Provisioner" qui vous permet d'activer la possibilité de création de volume.

📘 Ref: https://rancher.com/docs/k3s/latest/en/storage/

