#!/bin/bash
export KUBECONFIG=$PWD/../../config/k3s.yaml
NAMESPACE="training"
kubectl apply -f pod.yaml -n ${NAMESPACE}
