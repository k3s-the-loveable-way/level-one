#!/bin/bash
export KUBECONFIG=$PWD/../../config/k3s.yaml
# 🖐️ other namespace
NAMESPACE="training"
kubectl apply -f pod.redis.client.bis.yaml -n ${NAMESPACE}
