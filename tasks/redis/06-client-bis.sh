#!/bin/bash
export KUBECONFIG=$PWD/../../config/k3s.yaml
# 🖐️ other namespace
NAMESPACE="training"
kubectl exec -n ${NAMESPACE} -it redis-client -- /bin/bash

# To connect to redis-mother of database namespace
# from another namespace, use this notation:
# <service_name>.<namespace_name>

# connection:
# redis-cli -h redis-mother.database

# get firstname
# get lastname