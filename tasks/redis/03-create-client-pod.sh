#!/bin/bash
export KUBECONFIG=$PWD/../../config/k3s.yaml
NAMESPACE="database"
kubectl apply -f pod.redis.client.yaml -n ${NAMESPACE}
