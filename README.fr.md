# K3S pour les devs

> **Avertissement préalable**: tous mes scripts fonctionnent sous OSX, et très certainement sous Linux (je n'ai pas eu le temps de tout re-tester) et sous Windows, je pense que si vous utilisez git-bash cela devrait marcher (mais je n'ai pas de poste Windows pour vérifier)

## Création du Cluster (en moins de 2 minutes 🤞)

**Objectif**: Créer un cluster K3S de 3 nodes (noeuds) avec 3 VM Multipass

**K3S** est une version "légère" de K8S (orientée IOT), développée par **Rancher**

**Multipass** est un gestionnaire de VM très simple à utiliser créé par **Canonical**

### Prérequis

- Installer Multipass
- Installer `kubectl`, la CLI de Kubernetes

### Actions

- Montrer et expliquer `create-cluster.sh` 
- Lancer `create-cluster.sh`
- Montrer le contenu de `config` et le fichier `k3s.yaml` (qui sera utilisé par `kubectl` avec la variable `KUBECONFIG`)

### Quelques commandes

- Montrer `metrics-nodes.sh` + exécuter
- Montrer `get-pods-kube-system.sh` + exécuter
- Montrer `metrics-pods-system.sh` + exécuter

> Extrêment pratique quand on doit debuguer

- Montrer `describe.sh` + exécuter (sur **CoreDNS**)
- Montrer `logs.sh` + exécuter (sur **CoreDNS**)

**CoreDNS** sert de serveur DNS dans K3S (il existe d'autres composants pour faire ça comme kube-dns) 

**Traefik** est installé par défaut sur K3S pour servir d'**Ingress Controller**. **Traefik** est un reverse proxy http et un load balancer. Pour faire court, c'est lui qui va permettre d'accéder facilement aux webapps déployées sur notre cluster en nous simplifiant toute la partie réseau.


### Encore mieux: Un outil important: K9S

- **K9S** est un outil qui permet de gérer son cluster à partir d'un terminal
- **K9S** utilise la variable `KUBECONFIG`, donc le fichier de configuration `k3s.yaml`

- Montrer comment lancer `k9s.sh`  et expliquer **K9S**

> montrer les logs, describe, ... (sur **CoreDNS**)

## Déployons notre 1ère application

Alors, il y a diverses façons de déployer une webapp sur kube (d'ailleurs on ne déploie pas que des webapps), je vous montre celle que j'utilise (elle est perfectible), là je le fais à la main, après c'est bien de s'outiller avec une CI - Là c'est pour apprendre.

Aller dans `/apps/hello-js`

C'est une application **Node-js** toute simple (Express)

Avec un `Dockerfile` 🐳

Pour déployer une application sur kube, on a besoin de manifestes (des fichiers yaml), on peut concaténer les manifestes en un seul (c'est ce que je fais)

J'utilise des variables d'environnement dans mes scripts shell. je me suis créé un template de manifeste pour déployer, et j'utilise l'utilitaire **envsubst** pour substituer les valeurs des variables dans mon template.

### Allons voir le manifeste

- Ouvrir `deploy.template.yaml`

Pour déployer une application web, nous avons besoin de 3 parties:

- **Service**: un ensemble de pods qui travaillent ensemble (défini par un label `selector`) et ici j'expose le port de mon container
- **Deployment**: la rubrique déploiement permet de définir l'image Docker utilisée pour déployer l'application (et la registry Docker), mais aussi passer des variables d'environnement et bien d'autres choses comme le nombre de replicas dans un Replicaset (ensemble de pod)
- **Ingress**: l'Ingress, c'est ce qui permet de faire le routage: ici je lui précise le domaine pour accéder à mon service web

vous allez voir un peu plus loin que ce domaine prendra la forme suivante: [hello-js-master.192.168.64.85.nip.io](hello-js-master.192.168.64.85.nip.io)

Je vais utiliser le service (externe) **nip.io** qui me permet avec l'Ingress d'utiliser un domaine de la forme:

`adresse-ip-du-cluster.nip.io` et je pourrais pré-fixer par ce que je veux (donc des sous-domaines) car le mode wildcard est accepté.

C'est très pratique pour expérimenter.

Le fonctionnement de **nip.io** est tout bête: lors d'une requête DNS, le service va récupérer l'IP qui précède `nip.io` et renvoyer cette IP. 

Et ainsi on dispose d'une infinité d'entrée DNS qui pointent vers l'IP du reverse proxy **Traefik**.

Réf: [https://nip.io/](https://nip.io/)

### Mon script de déploiement (pour mes tests)

Si on simplifie, un déploiement sur kube, consiste en:

- "builder" une image de son application
- la "pousser" vers une registry Docker
- appliquer le fichier manifeste dans le cluster avec `kubectl`

<TODO>🖐️ Expliquer le script `deploy.sh`</TODO>

### On déploie

- `./deploy.sh`
- `./get-deployments.sh` (montrer)
- `./get-ingress.sh` (montrer)
- ouvrir dans un navigateur

### On scale

Il est facile de scaler (multiplier le nombre de pods)

- montrer `scale-to-3.sh`
- lancer `scale-to-3.sh`
- ouvrir dans un navigateur et faire des refreshs pour montrer que les 3 pods sont utilisés.

### Déployons une version de test de notre 1ère application

```bash
git checkout -b new-home
```

- faire une modification de la home page
- déployer
- `./get-ingress.sh`
- montrer la "prod" et la "preview"
- supprimer le déploiement: `./delete-deployment.sh`

```bash
git checkout master
git merge new-home
./deploy.sh
```

- montrer la "prod"

## Créer et utiliser un volume

Aller dans `tasks/create-volume` (🖐️ montrer le contenu des scripts)

- `./00-create-namespace.sh`
- **créer un volume**: `./01-create-volume-claim.sh`
  - 🖐️ montrer `pvc.yaml`
- **créer un pod avec une image qui contient des outils et qui pointe sur le volume**: `./02-create-volume-pod.sh` 
  - 🖐️ montrer `pod.yaml`
- **aller faire un tour dans le pod et créer un fichier dans `/data`**: `./03-pod-shell.sh`
- **créer un autre pod qui pointe sur le même volume**: `./04-create-volume-pod.sh`
  - 🖐️ montrer `pod.bis.yaml`
- **aller faire un tour dans le pod et vérifier que le fichier est toujours dans `/data`**:`./05-pod-shell.sh`

## Installer une base Redis

Aller dans `tasks/redis` (🖐️ montrer le contenu des scripts)

- `./00-create-namespace.sh`
- `./01-create-volume-claim.sh`
- **Installer la base Redis + Volume**: `./02-create-redis-pod.sh` 
  - 🖐️ montrer `redis.yaml`
- **Créer un pod utilitaire client Redis**: `./03-create-client-pod.sh`
  - 🖐️ montrer `pod.redis.client.yaml`
- **Aller dans le shell du pod**: `./04-client-shell.sh`
  - 🖐️ Créer des enregistrement dans la base:
  ```bash
  # connection:
  redis-cli -h redis-mother

  set firstname bob
  set lastname morane
  get firstname
  get lastname
  ```
- **Créer un pod utilitaire client Redis (DANS UN AUTRE NEAMESPACE)**: `./05-create-client-bis-pod.sh`
- **Aller dans le shell du pod**`./06-client-bis.sh`
  - 🖐️ se connecter à la base à partir d'un autre namespace
  ```bash
  # To connect to redis-mother of database namespace
  # from another namespace, use this notation:
  # <service_name>.<namespace_name>

  # connection:
  redis-cli -h redis-mother.database

  get firstname
  get lastname
  ```

## Déployer une application qui utilise la base Redis

Maintenant que nous avons une base Redis, nous allons déployer une application qui l'utilise

- Aller dans `/apps/hey-js`
- Regarder le code de `index.js`
- Regarder le manifeste `deploy.template.yamls` (**et voir comment passer des variables d'environnement**)
- Regarder le script de déploiement `deploy.sh` avec les valeurs concernat la base Redis
- Déployer
- 🖐️ Créer des enregistrements: `./call-js-service.post.sh`
- Aller voir les logs du pod dans K9S
- 🖐️ Lister les enregistrements: `./call-js-service.get.sh`

## Jouer avec les configmaps

- Aller dans `/apps/yo-js`
- Regarder le code de `index.js` +  `function.js`
- Regarder le manifeste `deploy.template.yamls` (**et voir la partie volume et configmap**)
- Déployer
- Créer une configmap (changer le code)
- Appeler le service
- Modifier et redéployer la configmap
- Appeler le service



