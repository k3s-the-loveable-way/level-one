#!/bin/sh
DOCKER_USER="k33g"
IMAGE_NAME="my-little-tools-img"
TAG="0.0.2"
IMAGE="${DOCKER_USER}/${IMAGE_NAME}:${TAG}"

# === Build and Push to Docker Hub ===
docker build -t ${IMAGE_NAME} .
docker tag ${IMAGE_NAME} ${IMAGE}
docker push ${IMAGE}

