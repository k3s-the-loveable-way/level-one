#!/bin/sh
export KUBECONFIG=$PWD/../config/k3s.yaml
eval $(cat $PWD/../vm.config)
# use the main vm
vm_name=${vm_name}-1

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
export SUB_DOMAIN="${IP}.nip.io"
export DOMAIN="alertmanager.${SUB_DOMAIN}"

envsubst < ./ingress-alertmanager-template.yaml  > ./ingress-alertmanager.yaml 
kubectl apply -f ingress-alertmanager.yaml 

echo "http://${DOMAIN}"

