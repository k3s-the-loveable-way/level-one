#!/bin/sh
export KUBECONFIG=$PWD/../config/k3s.yaml
eval $(cat $PWD/../vm.config)
# use the main vm
vm_name=${vm_name}-1

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
export SUB_DOMAIN="${IP}.nip.io"
export DOMAIN="prometheus.${SUB_DOMAIN}"

envsubst < ./ingress-prometheus-template.yaml  > ./ingress-prometheus.yaml 
kubectl apply -f ingress-prometheus.yaml 

echo "http://${DOMAIN}"

