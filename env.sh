#!/bin/sh
# use it like that: . ./env.sh; ./deploy "🎉 first deploy"  
# use it like that: . ./env.sh; ./scale-to-3  

export KUBECONFIG=$PWD/config/k3s.yaml
eval $(cat $PWD/vm.config)
# use the main vm
vm_name=${vm_name}-1

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')
export SUB_DOMAIN="${IP}.nip.io"
export DOCKER_USER="k33g"